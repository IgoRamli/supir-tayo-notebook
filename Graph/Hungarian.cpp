int a[MAXN + 5][MAXN + 5],u[MAXN + 5],v[MAXN + 5];
bool used[MAXN + 5];
int p[MAXN + 5],way[MAXN + 5];
int minv[MAXN + 5];
int ans[MAXN + 5];
 
// kalo sizenya n < m, pad sampe sama.
// ini hungarian min cost, kalo mo max cost kasih -1
int Assign(){
    for(int i = 1 ; i <= n ; i++){
        p[0] = i;
        int j0 = 0;
        for(int j = 0 ; j <= m ; j++)
            minv[j] = 1000000000,used[j] = 0;
        do{
            used[j0] = 1;
            int i0 = p[j0],delta = 1000000000, j1;
            for(int j = 1 ; j <= m ; j++)
                if(!used[j]){
                    int cur = a[i0][j] - u[i0] - v[j];
                    if(cur < minv[j])
                        minv[j] = cur,way[j] = j0;
                    if(minv[j] < delta)
                        delta = minv[j], j1 = j;     
                }
            for(int j = 0 ; j <= m ; j++)
                if(used[j])
                   u[p[j]] += delta,  v[j] -= delta; 
                else
                   minv[j] -= delta;  
            j0 = j1;           
        }while(p[j0] != 0); 
        
        do{
            int j1 = way[j0];
            p[j0] = p[j1];
            j0 = j1;
        }while(j0);   
    }
    
    for(int i = 1 ; i <= n ; i++)
        ans[p[i]] = i;
    
    int ret = 0;
    for(int i = 1 ; i <= n ; i++)
        ret += val[i][ans[i]];// person i is matched with job ans[i]
           
    return ret;    
}
