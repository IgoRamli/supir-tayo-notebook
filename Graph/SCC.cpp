int low[MAXN], num[MAXN], vis[MAXN];
vector<int> stek, adj[MAXN];
int cnt;
 
void tarjan(int v) {
  low[v] = num[v] = cnt++;
  vis[v] = 1; // mark as inside stack
  stek.pb(v);
  
  for(int nex : adj[v]) {
    if(vis[nex] == 0) tarjan(nex);
    if(vis[nex] == 1) low[v] = min(low[v],low[nex]);
  }
  
  if(low[v] == num[v]) {
    while(1) {
      int u = stek.back(); stek.pop_back();
      vis[u] = 2; // mark as out of stack and has been processed
      if(u == v) break;
    }
  }
}