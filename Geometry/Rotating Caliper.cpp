// Cited from Stanford Team Notebook
inline pii FindAllPodal_And_GetMaxPair() {
	LL ans = 0;
	int P = 0, Q = 0;
	int sizeU = U.size();
	int sizeL = L.size();
	int i = 0;
	int j = sizeL-1;
	while(i < sizeU-1 || j > 0) {
		LL tmpdist = dist(U[i], L[j]);
		if (tmpdist > ans) {
			P = i, Q = j;
			ans = tmpdist;
		}
		if (i == sizeU-1) j--;
		else if (j == 0) i++;
		else if ((U[i+1].y - U[i].y)*(L[j].x - L[j-1].x) > (U[i+1].x - U[i].x)*(L[j].y - L[j-1].y)) i++;
		else j--;
	}
	return make_pair(U[P].pos, L[Q].pos);
}
