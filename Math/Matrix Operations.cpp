typedef double mat[MAX][MAX];

/* LU-Decomposition
Decomposes an n x n matrix A into LU.*/
void LU_decompose(int n, mat A, mat LU) {
  REP(i, n) REP(j, N) LU[i][j] = A[i][j];
  for (int i = 0; i < n; i++) {
    for (int j = i+1; j < n; j++) {
    double temp = LU[j][i]/LU[i][i];
    for (int k = i; k < n; k++)
        LU[j][k] -= LU[i][k] * temp;
      LU[j][i] = temp;
    }
  }
}

/*Back Substitution
Solves matrix equation Ux = b.*/
void back_substitute(int n, mat U, double x[], double b[]){
  for (int i = n-1; i >= 0; i--){
    x[i] = b[i];
    for (int j = i+1; j < n; j++)
      x[i] -= x[j]*U[i][j];

    x[i] /= U[i][i];
  }
}

/*Forward Substitution
Solves matrix equation Lx = b.*/
void forward_substitute(int n, mat L, double x[], double b[]){
  for (int i = 0; i < n; i++){
    x[i] = b[i];
    for (int j = 0; j < i; j++)
      x[i] -= x[j]*L[i][j];

/*Linear System
Solves matrix equation Ax = b.*/
void solve(int n, mat A, double x[], double b[]){
  mat LU;
  REP(i, n) REP(j, n) LU[i][j] = A[i][j];
  for (int i = 0; i < n; i++)
    LU[i][n] = b[i];
  for (int i = 0; i < n; i++){
    int pivot = i;
    for (int j = i+1; j < n; j++)
      if (fabs(LU[j][i]) > fabs(LU[pivot][i]))
        pivot = j;
    for (int j = i; j <= n; j++)
      swap(LU[i][j], LU[pivot][j]);
    for (int j = i+1; j < n; j++){
      double temp = LU[j][i]/LU[i][i];
      for (int k = i; k <= n; k++)
        LU[j][k] -= LU[i][k] * temp;
      LU[j][i] = temp;
    }
  }
  for (int i = 0; i < n; i++)
    b[i] = LU[i][n];
  back_substitute(n, LU, x, b);
}

/*Matrix Inverse
Compute the inverse Ac of matrix A.*/
void inverse(int n, mat A, mat Ac) {
  mat LU;
  LU_decompose(n, A, LU);
  double x[MAX], e[MAX], y[MAX];
  REP(j, n) {
    REP(i, n) e[i] = 0;
    e[j] = 1;
    forward_substitute(n, LU, y, e);
    back_substitute(n, LU, x, y);
    REP(i, n) Ac[i][j] = x[i];
  }
}

/*Matrix Determinant
Returns the determinant of matrix A.*/
double det(int n, mat A) {
  mat LU;
  LU_decompose(n, A, LU);
  double res = 1;
  REP(i, n) res *= LU[i][i];
  return res * (n % 2 ? -1 : 1);
}

/*
  * Find inverse using gaussian elimination:
    - Pad N more column, which represents identity matrix
    - Do gaussian elimination until original matrix become identity matrix
    - The padded matrix is now the inverse
  
  * Find determinant using gaussian elimination:
    - Do gaussian elimination. keep variable, say d = 1
    - if swap row operation, d = -1 * d
    - if multiply row by C, d = C * d
    - at the end, determinant is (product of main diagonal) / d
*/
