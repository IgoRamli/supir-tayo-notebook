# Supir Tayo Team Notebook

Repository for our team notebook in preparation for WF 2019. We used [codes2pdf](https://github.com/Erfaniaa/codes2pdf) to compile the codes and latex files into a pdf file.

Usual command (run on root of repo):

```bash
codes2pdf . --author="Supir Tayo - Universitas Indonesia" --initials="Supir Tayo - Universitas Indonesia"
```

Use at your own risk.