/* Generate Z Array
Z[i] is maximum R such that S[0..R-1] == S[i..i+R-1]
Usage:
1. Everything that can be done with KMP
2. Longest Common Prefix in linear time
*/

int L = 0, R = 0;
for (int i = 1; i < n; i++) {
   if (i > R) {
        L = R = i;
        while (R < n && s[R-L] == s[R]) R++;
        z[i] = R-L; R--;   
   } else {
     int k = i-L;
     if (z[k] < R-i+1) z[i] = z[k];
     else {
       L = i;
       while (R < n && s[R-L] == s[R]) R++;
       z[i] = R-L; R--;
     }
   }
}
