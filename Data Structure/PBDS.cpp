#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;
using namespace __gnu_pbds;

struct PBDS
{
	typedef tree< // The definition of PBDS
		pair<int, int>, // Key class
		null_type, // Value class
		less<pair<int, int>>, // Compare class, functioning as key comaprison
		rb_tree_tag, // Type of tree to be used. Red Black Tree (rb_tree_tag) is recommended
		tree_order_statistics_node_update> pbds;
	
	pbds t;
	int timer;
	
	PBDS(){timer = 0;}
	void insert(int x){t.insert({x, timer++});}
	
	// strictly lower
	int lower(int x){return t.order_of_key({x, -1});}
	
	//make sure x exists
	void del(int x){
		pair<int, int> tmp = (*t.find_by_order(lower(x)));
		t.erase(tmp);
	}
	
	// strictly higher
	int higher(int x){
		int tmp = lower(x+1);
		return (int(t.size()) - tmp);
	}
};
