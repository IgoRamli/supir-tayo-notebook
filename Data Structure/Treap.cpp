class Treap{
	struct Node{
		int prior, key, siz;
		Node *l, *r;
		Node(){}
		Node(int _prior, int _key){
			prior = _prior; key = _key; siz = 1;
			l = r = NULL;
		}
	};
	typedef Node * pnode;
	int cnt(pnode t){
		return (!t) ? 0 : t->siz;
	}
	void upd_siz(pnode t){
		if(t) t->siz = 1 + cnt(t->l) + cnt(t->r);
	}
	pnode root = NULL;

	void insert(pnode &t, pnode newNode){
		if(!t) t = newNode;
		else if(t->prior > newNode->prior) insert((t->key > newNode->key) ? t->l:t->r, newNode);
		else split(t, newNode->key, newNode->l, newNode->r), t = newNode;
		upd_siz(t);
	}
	void del(pnode &t, int key){
		if(!t) return;
		else if(t->key == key) merge(t, t->l, t->r);
		else del((t->key > key) ? t->l:t->r, key);
		upd_siz(t);
	}
	int getElem(pnode t, int pos, int add = 0){
		int cur_idx = 1 + add + cnt(t->l);
		if(cur_idx == pos) return t->key;
		else if(cur_idx > pos) return getElem(t->l, pos, add);
		else return getElem(t->r, pos, cur_idx);
	}
	bool exist(pnode t, int key){
		if(!t) return false;
		if(t->key == key) return true;
		return (t->key > key) ? exist(t->l, key) : exist(t->r, key);
	}
	void print(pnode t){
		if(!t) return;
		print(t->l); printf("(%d)", t->key); print(t->r);
	}

	public:
		Treap(){srand(time(NULL));}
		void split(pnode t, int key, pnode &l, pnode &r){ // l will contain all elements < key
			if(!t) l = r = NULL;
			else if(t->key >= key) split(t->l, key, l, t->l), r = t;
			else split(t->r, key, t->r, r), l = t;
			upd_siz(t);
		}
		void merge(pnode &t, pnode l, pnode r){
			if(!l || !r) t = (l) ? l : r;
			else if(l->prior > r->prior){ merge(l->r, l->r, r), t = l; }
			else{ merge(r->l, l, r->l), t = r; }
			upd_siz(t);
		}
		void insert(int key){
			if(exist(root, key)) return;
			pnode tmp = new Node(rand(), key);
			insert(root, tmp);
		}
		void del(int key){
			del(root, key);
		}
		int getElem(int pos){
			return (cnt(root) < pos) ? -INF:getElem(root, pos);
		}
		int getSmall(int key){
			pnode t1, t2;
			split(root, key, t1, t2);
			int tmpAns = cnt(t1);
			merge(root, t1, t2);
			return tmpAns;
		}
};